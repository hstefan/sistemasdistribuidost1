package com.hstefan.distrib.t1.producer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

import com.hstefan.distrib.t1.common.Message;
import com.hstefan.distrib.t1.common.Message.MessageType;

public class Producer implements Runnable {

	private int mServerPort;
	private String mServerName;
	private int mStackSize;
	
	public Producer(int serverPort, String serverName, int stackSize) {
		this.mServerPort = serverPort;
		this.mServerName = serverName;
		this.mStackSize = stackSize;
	}

	@Override
	public void run() {
		Random rand = new Random();
		try {
			InetAddress ip = InetAddress.getByName(mServerName);
			Socket socket = new Socket(ip, mServerPort);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			
			for(int i = 0; i < mStackSize; ++i) {
				oos.writeObject(new Message(MessageType.PUSH, (1 + rand.nextInt(Short.MAX_VALUE - 1))));
			}
			
			int lockval[] = new int[1];
			lockval[0] = 0;
			oos.writeObject(new Message(MessageType.LOCK_STACK, 0, lockval));
			
			oos.writeObject(new Message(MessageType.KILL_SERVER));
			System.out.println("Producer ended, waiting for server to stop the process.");
			
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			Object o = ois.readObject();
			if(o instanceof Message) {
				Message m = (Message)o;
				if(m.type == MessageType.KILL_SERVER_ACK) {
					System.out.println("Halting.");
					return;
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}