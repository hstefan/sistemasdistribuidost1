package com.hstefan.distrib.t1.common;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Message implements Serializable {
	public enum MessageType {
		PUSH, POP, POP_RETURN, KILL_SERVER, KILL_SERVER_ACK, LOCK_STACK
	};
	
	public MessageType type;
	public int val;
	public int[] ret;
	
	public Message(MessageType type, int val) {
		this.type = type;
		this.val = val;
	}
	
	public Message(MessageType type, int val, int[] ret) {
		this.type = type;
		this.val = val;
		this.ret = ret;
	}
	
	public Message(MessageType type) {
		this.type = type;
		this.val = 0;
	}
	
	public Message(MessageType type, int[] ret) {
		this.type = type;
		this.val = 0;
		this.ret = ret;
	}
}
