package com.hstefan.distrib.t1.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import com.hstefan.distrib.t1.common.Message;
import com.hstefan.distrib.t1.common.Message.MessageType;

public class Server implements Runnable {	
	private ServerSocket mServerSocket;
	private CriticalDataQueue<Integer> mStack;
	
	public static final int NOTHING_TO_CONSUME = -404;
	
	public Server(int port, String name) {
		this.mStack = new CriticalDataQueue<Integer>();
		try {
			mServerSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while(true) {
				Socket s = mServerSocket.accept();
				new ServerThread(s).start();
				System.out.println("New client connected: " + s.getInetAddress().getHostAddress());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private class ServerThread extends Thread {
		private Socket mSocket;
		
		public ServerThread(Socket s) {
			mSocket = s;
		}
		
		public void run() {
			try {
				ObjectInputStream ois = new ObjectInputStream(mSocket.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(mSocket.getOutputStream());
				
				Message m = null;
				Object o = null;
				while(mSocket.isConnected()) {
					o = ois.readObject();
					if(o instanceof Message) {
						m = (Message) o;
						if(m.type == MessageType.KILL_SERVER) {
							oos.writeObject(new Message(MessageType.KILL_SERVER_ACK));
							ois.close();
							oos.close();
							System.out.println("Connection reset: " + 
									mSocket.getInetAddress().getHostAddress());
							mSocket.close();
							break;
						} else if(m.type == MessageType.PUSH) {
							mStack.push(m.val);
						} else if(m.type == MessageType.POP) {
							int ret[] = new int[m.val];
							if(m.val > 0) {
								Integer p = null;
								for(int i = 0; i < m.val; ++i) {
									p = mStack.pop();
									if(p == null) {
										ret[i] = NOTHING_TO_CONSUME;
										break;
									} else {
										ret[i] = p;
									}
								}
							}
							oos.writeObject(new Message(MessageType.POP_RETURN, ret));
						} else if(m.type == MessageType.LOCK_STACK) {
							int lockval = m.ret[0];
							mStack.setLock(true, lockval);
						}
					} else {
						System.out.println("Unexpected object found.");
					}
				}
			} catch (SocketException e) {
				if(e instanceof SocketException) {
					System.out.println("Connection hanged up unexpectedly: " + 
						mSocket.getInetAddress().getHostAddress());
					System.out.println(mStack.size());
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} 
		}
	}
}
