package com.hstefan.distrib.t1.server;

import java.util.ArrayList;

public class CriticalDataQueue<T> {
	private ArrayList<T> mList;
	private boolean mLocked;
	private boolean mLockSet;
	private T mLockVal;
	
	public CriticalDataQueue() {
		mList = new ArrayList<T>();
		mLocked = false;
		mLockSet = false;
		mLockVal = null;
	}
	
	public synchronized boolean empty() {
		return mList.isEmpty();
	}

	public synchronized T pop() {
		if(mList.isEmpty() && mLockSet) {
			mLocked = true;
		} if (mLocked) {
			return mLockVal;
		}
		return mList.isEmpty()? null : mList.remove(0);
	}

	public synchronized void push(T item) {
		if(!mLocked) {
			mList.add(item);
		}
	}
	
	public synchronized int size() {
		return mList.size();
	}
	
	public synchronized T ifNotEmptyPop() {
		if(!empty()) {
			return pop();
		} else {
			return null;
		}
	}
	
	public synchronized void setLock(boolean state, T val) {
		mLockVal = val;
		mLockSet = state;
	}
}
