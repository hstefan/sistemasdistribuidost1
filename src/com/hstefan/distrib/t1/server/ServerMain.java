package com.hstefan.distrib.t1.server;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

public class ServerMain {
	public static void main(String[] args) {
		Options opts = new Options();
		opts.addOption("h", "hostname", true, "The hostname where the server will run.");
		opts.addOption("p", "port", true, "The port that the server will listen to.");
		opts.addOption("s", "stack-size", true, "The target size of the produced stack.");
		@SuppressWarnings("static-access")
		Option help = OptionBuilder.withLongOpt("help").withDescription("Prints this message.").create();
		opts.addOption(help);
		
		String hostname = "localhost";
		int port = 8000;
		
		CommandLineParser parser = new PosixParser();
		try {
			CommandLine cmd = parser.parse(opts, args);
			if(cmd.hasOption("help")) {
				HelpFormatter formater = new HelpFormatter();
				formater.printHelp("java com.hstefan.distrib.t1.server.ServerMain", opts);
				return;
			}
			if(cmd.hasOption("h")) {
				hostname = cmd.getOptionValue("h");
				
			}
			if(cmd.hasOption("p")) {
				port = Integer.parseInt(cmd.getOptionValue("p"));
			}
		} catch (ParseException e) {
			System.out.println("Failed to parse arguments.");
		}
		
		new Thread(new Server(port, hostname)).start();
	}
}
