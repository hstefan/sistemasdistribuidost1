package com.hstefan.distrib.t1.server;

import java.io.IOException;
import java.net.ServerSocket;

public class ConnectionListener extends Thread {
	private ServerSocket mServerSocket;
	
	public ConnectionListener(ServerSocket serverSocket) {
		mServerSocket = serverSocket;
	}
	
	@Override
	public void run() {
		try {
			while(true) {
				System.out.print("Listening to connection... ");
				mServerSocket.accept();
				System.out.println("Connected!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
