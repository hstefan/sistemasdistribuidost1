package com.hstefan.distrib.t1.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import com.hstefan.distrib.t1.common.Message;
import com.hstefan.distrib.t1.common.Message.MessageType;
import com.hstefan.distrib.t1.server.Server;

public class Consumer implements Runnable {
	
	private int mServerPort;
	private String mServerName;
	private int mNumPops;
	
	
	public Consumer(int serverPort, String serverName, int popsPerMessage) {
		this.mServerPort = serverPort;
		this.mServerName = serverName;
		this.mNumPops = popsPerMessage;
	}

	@Override
	public void run() {
		int acc = 0;
		try {
			InetAddress ip = InetAddress.getByName(mServerName);
			Socket socket = new Socket(ip, mServerPort);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			while(true) {
				oos.writeObject(new Message(MessageType.POP, mNumPops));
				Object ret = ois.readObject();
				if(ret instanceof Message) {
					Message msgRet = (Message) ret; 
					boolean found_zero = false;
					if(msgRet.type == MessageType.POP_RETURN) {
						for(int val : msgRet.ret) {
							if(val == Server.NOTHING_TO_CONSUME) {
								break;
							} else if(val == 0) {
								found_zero = true;
								break;
							} else {
								acc += val;
							}
						}
					}
					if(found_zero) 
						break;
				}
			}
			
			oos.writeObject(new Message(MessageType.KILL_SERVER));
			System.out.println("Consumer " + super.toString() + " has accumulated " + acc);
			
			Object o = ois.readObject();
			if(o instanceof Message) {
				Message m = (Message)o;
				if(m.type == MessageType.KILL_SERVER_ACK) {
					System.out.println("Halting.");
					return;
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
