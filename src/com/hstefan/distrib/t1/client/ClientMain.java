package com.hstefan.distrib.t1.client;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

public class ClientMain {

	
	public static void main(String[] args) {
		Options opts = new Options();
		opts.addOption("h", "hostname", true, "The hostname where the server is running.");
		opts.addOption("p", "port", true, "The port which the server listens to.");
		opts.addOption("c", "consumers", true, "The number of Consumers");
		opts.addOption("m", "pops-per-message", true, "Number of pops per message send by consumer.");
		@SuppressWarnings("static-access")
		Option help = OptionBuilder.withLongOpt("help").withDescription("Prints this message.").create();
		opts.addOption(help);
		
		String hostname = "localhost";
		int port = 8000;
		int n = 1;
		int pops = 20;
		
		CommandLineParser parser = new PosixParser();
		try {
			CommandLine cmd = parser.parse(opts, args);
			if(cmd.hasOption("help")) {
				HelpFormatter formater = new HelpFormatter();
				formater.printHelp("java com.hstefan.distrib.t1.server.ClientMain", opts);
				return;
			}
			if(cmd.hasOption("h")) {
				hostname = cmd.getOptionValue("h");
				
			}
			if(cmd.hasOption("p")) {
				port = Integer.parseInt(cmd.getOptionValue("p"));
			}
			if(cmd.hasOption("c")) {
				n = Integer.parseInt(cmd.getOptionValue("c"));
			}
			if(cmd.hasOption("m")) {
				pops = Integer.parseInt(cmd.getOptionValue("m"));
			}
		} catch (ParseException e) {
			System.out.println("Failed to parse arguments.");
		}
		
		for(int i = 0; i < n; ++i) {
			System.out.println("Spawning consumer #" + i);
			new Thread(new Consumer(port, hostname, pops)).start();
		}
	}
}
